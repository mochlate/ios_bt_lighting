//
//  ViewController.swift
//  BTLighting
//
//  Created by Josh Schneider on 2/4/16.
//  Copyright © 2016 Josh Schneider. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    var manager : CBCentralManager!
    var miBand : CBPeripheral!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager = CBCentralManager(delegate: self, queue: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        
        
        print("Name: \(peripheral.name)")
        if(peripheral.name == "MI") {
            
            self.miBand = peripheral
            self.miBand.delegate = self
            manager.stopScan()
            manager.connectPeripheral(self.miBand, options: nil)
            
        }
        
    }
    
    
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
        
        
        if let servicePeripherals = peripheral.services as [CBService]!
        {
            for service in servicePeripherals
            {
                peripheral.discoverCharacteristics(nil, forService: service)
            }
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
        
        if let characterArray = service.characteristics as [CBCharacteristic]!
        {
            for cc in characterArray
            {
                if(cc.UUID.UUIDString == "FF06") {
                    print("Schritte gefunden")
                    peripheral.readValueForCharacteristic(cc)
                }
            }
            
        }
        
    }
    
    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        
        if(characteristic.UUID.UUIDString == "FF06") {
            
            let value = UnsafePointer<Int>(characteristic.value!.bytes).memory
            print("Gelaufene Schritte heute: \(value)")
        }
        
    }
    
    
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        
    }
    
    
    func centralManagerDidUpdateState(central: CBCentralManager) {
        
        var msg = ""
        switch (central.state) {
            
        case .PoweredOff:
            msg = "Bluetooth leider ausgeschaltet"
        case .PoweredOn:
            msg = "Bluetooth ist eingeschaltet"
            manager.scanForPeripheralsWithServices(nil, options: nil)
        case .Unsupported:
            msg = "Bluetooth nicht verfügbar"
        default: break
        }
        print("STAT: \(msg)")
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

